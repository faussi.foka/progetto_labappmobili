# Progetto_LabAppMobili

Progetto consegnato con valutazione per il corso di Laboratorio Applicazioni Mobili, dell'università di Bologna.
La tecnologia utilizzata è Android Studio.

## ServerTombola

Cartella contenente il lato Server del progetto, è un'applicazione Android.

## Tombola

Cartella contenente il lato Client del progetto, utilizzabile dall'utente. È un'applicazione Android.

## testo_progetto.pdf

Contiene le richieste del progetto.

## FOKA.pdf

File pdf contenente una descrizione dettagliata del progetto.
Dal PowerPoint è possibile visualizzare una descrizione più sintetizzata dell'intero progetto.
